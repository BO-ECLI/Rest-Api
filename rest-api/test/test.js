/*global console, XMLSerializer, Document*/
var request = document.getElementById("request"),
    response = document.getElementById("response"),
    form = document.getElementById("form"),
    xml = document.getElementById("xml"),
    json = document.getElementById("json"),
    text = document.getElementById("text"),
    languageSelect = document.getElementById("language-select"),
    language = document.getElementById("language"),
    jurisdictionSelect = document.getElementById("jurisdiction-select"),
    jurisdiction = document.getElementById("jurisdiction"),
    inputEcli = document.getElementById("input-ecli"),
    annotatedText = document.getElementById("annotated-text"),
    errorDiv = document.getElementById("error");

function empty(e) {
    "use strict";
    while (e.firstChild) {
        e.removeChild(e.firstChild);
    }
}
function clear() {
    "use strict";
    empty(request);
    empty(response);
    empty(annotatedText);
    empty(errorDiv);
}
function setFields(set) {
    "use strict";
    set("language", language.value || "IT");
    set("jurisdiction", jurisdiction.value || "");
    set("input-ecli", inputEcli.value);
}
function makeXml() {
    "use strict";
    var ns = "http://boecli.eu/engine/0.1",
        doc = document.implementation.createDocument(ns, "text-input", null);
    function e(p, ln) {
        return p.appendChild(doc.createElementNS(ns, ln));
    }
    function a(p, ln, v) {
        p.setAttribute(ln, v);
    }
    var root = doc.documentElement;
    setFields(function (name, value) {
        if (value !== "") {
            a(root, name, value);
        }
    });
    var t = e(root, "text");
    t.appendChild(doc.createTextNode(text.value));
    return root;
}
function serialize(dom) {
    "use strict";
    var s = new XMLSerializer();
    return s.serializeToString(dom);
}
function makeJson() {
    "use strict";
    var data = {
        "text": text.value
    };
    setFields(function (name, value) {
        if (value !== "") {
            data[name] = value;
        }
    });
    return data;
}
function error(msg) {
    "use strict";
    empty(errorDiv);
    errorDiv.appendChild(document.createTextNode(msg));
}
function getAnnotatedText(doc) {
    "use strict";
    var e = doc.documentElement.firstElementChild;
    while (e) {
        if (e.localName === "annotated-text") {
            return e.textContent;
        }
        e = e.nextElementSibling;
    }
    return "";
}
function setAnnotatedText(text) {
    "use strict";
    text.split("\n").forEach(function (t) {
        var p = document.createElement("p");
        p.innerHTML = t;
        annotatedText.appendChild(p);
    });
}
function doXml(path, data, callback) {
    "use strict";
    var xhr = new XMLHttpRequest(),
        method = "GET";
    if (data !== null) {
        method = "POST";
        data = serialize(data);
    }
    function write(field, m) {
        empty(field);
        var msg;
        msg = method + " " + path;
        if (m) {
            msg += "\n" + m;
        }
        msg = vkbeautify.xml(msg, 4)
        field.appendChild(document.createTextNode(msg));
    }
    write(request, data);
    xhr.open(method, path, true);
    xhr.setRequestHeader("Accept", "text/xml");
    xhr.setRequestHeader("Content-Type", "text/xml");
    function handleResult() {
        if (xhr.readyState === 4) {
            if (xhr.status < 200 || xhr.status >= 300) {
                write(response, xhr.responseText);
                if (callback) {
                    callback(xhr.statusText);
                }
                return;
            }
            setAnnotatedText(getAnnotatedText(xhr.responseXML));
            write(response, xhr.responseText);
            if (callback) {
                callback(null, xhr.responseXML);
            }
        }
    }
    xhr.onreadystatechange = handleResult;
    xhr.send(data);
}
function doJson(path, data, callback) {
    "use strict";
    var xhr = new XMLHttpRequest(),
        method = "GET";
    if (data !== null) {
        method = "POST";
        data = JSON.stringify(data, null, "  ");
    }
    function write(field, m) {
        empty(field);
        var msg;
        msg = method + " " + path;
        if (m) {
            msg += "\n" + m;
        }
        field.appendChild(document.createTextNode(msg));
    }
    write(request, data);
    xhr.open(method, path, true);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/json");
    function handleResult() {
        if (xhr.readyState === 4) {
            if (xhr.status < 200 || xhr.status >= 300) {
                write(response, xhr.responseText);
                if (callback) {
                    callback(xhr.statusText);
                }
                return;
            }
            var j;
            try {
                j = JSON.parse(xhr.responseText);
            } catch (e) {
                error("Invalid JSON. " + e);
                return write(response, xhr.responseText);
            }
            if (j["annotated-text"]) {
                setAnnotatedText(j["annotated-text"]);
            }
            write(response, JSON.stringify(j, null, "  "));
            if (callback) {
                callback(null, j);
            }
        }
    }
    xhr.onreadystatechange = handleResult;
    xhr.send(data);
}
function run() {
    "use strict";
    clear();
    var path = "/entry-point/run";
    if (xml.checked) {
        doXml(path, makeXml());
    } else {
        doJson(path, makeJson());
    }
}
function addTextBoxHandler(input) {
    "use strict";
    input.onchange = function () {
        run();
    };
    input.onkeyup = function (event) {
        if (event.keyCode === 13) {
            run();
        }
    };
}
function loadExample(example) {
    "use strict";
    language.value = example.parentNode.getAttribute('data-lang')
    const p = example.querySelectorAll('p')
    if (p.length === 1) {
        inputEcli.value = ""
        text.value = p.item(0).innerText
    } else {
        inputEcli.value = p.item(0).innerText
        text.value = p.item(1).innerText
    }
    run()
}
function hookupExamples() {
    "use strict";
    document.querySelectorAll('.example').forEach(example => {
        example.addEventListener('click', () => loadExample(example))
    })
}
function init() {
    "use strict";
    xml.onclick = run;
    json.onclick = run;
    text.onkeypress = function (e) {
        if (e.keyCode === 13 && e.ctrlKey) {
            run();
        }
    };
    addTextBoxHandler(inputEcli);
    hookupExamples();
}
init();
