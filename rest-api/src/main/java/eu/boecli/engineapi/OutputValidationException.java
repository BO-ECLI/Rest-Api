package eu.boecli.engineapi;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class OutputValidationException extends WebApplicationException {

	private static final long serialVersionUID = -6140930137911476978L;

	public OutputValidationException(String message) {
		super(Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.entity(message).type("text/plain").build());
	}
}