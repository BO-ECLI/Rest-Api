package eu.boecli.engineapi;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class InputValidationException extends WebApplicationException {
	private static final long serialVersionUID = 5791082946989574681L;

	public InputValidationException(String message) {
		super(Response.status(Response.Status.BAD_REQUEST).entity(message)
				.type("text/plain").build());
	}
}