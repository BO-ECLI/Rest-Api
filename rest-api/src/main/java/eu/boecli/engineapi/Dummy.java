package eu.boecli.engineapi;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.boecli.common.CommonCaseLawAuthorities;
import eu.boecli.common.CommonCaseLawTypes;
import eu.boecli.common.CommonLegislationAliases;
import eu.boecli.common.Subject;
import eu.boecli.engineapi.schema.LegalIdentifier;
import eu.boecli.engineapi.schema.LegalReference;
import eu.boecli.engineapi.schema.References;

public class Dummy {
	public static References getDummyLegalReferences() {

		DatatypeFactory datatypeFactory;
		try {
			datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new ServerException(e);
		}
		References dummyLegalReferences = new References();

		LegalReference r = new LegalReference();
		LegalIdentifier li;
		r.setText("Cass., trib., 5 dicembre 2005 n. 26403");
		r.setAuthority("IT_CASS");
		r.setType(CommonCaseLawTypes.JUDGMENT.toString());
		r.setSection("T");
		r.setSubject(Subject.CIVIL.toString());
		r.setYear(datatypeFactory.newXMLGregorianCalendar("2005"));
		r.setNumber("26403");
		XMLGregorianCalendar xmlDate = datatypeFactory.newXMLGregorianCalendar("2005-12-05");
		r.setDay(xmlDate);
		r.setMonth(xmlDate);
		r.setIsCaseLawReference(true);

		li = new LegalIdentifier();
		li.setCode("ECLI:IT:CASS:2005:26403CIV");
		li.setType("ECLI");
		r.getLegalIdentifier().add(li);
		dummyLegalReferences.getLegalReference().add(r);

		r = new LegalReference();
		r.setText(
				"Corte Costituzionale con la sentenza n. 51 del 18 febbraio 1992");
		r.setAuthority("IT_COST");
		r.setType(CommonCaseLawTypes.JUDGMENT.toString());
		r.setYear(datatypeFactory.newXMLGregorianCalendar("1992"));
		r.setNumber("51");
		xmlDate = datatypeFactory.newXMLGregorianCalendar("1992-02-18");
		r.setDay(xmlDate);
		r.setMonth(xmlDate);
		r.setYear(xmlDate);
		r.setIsCaseLawReference(true);
		li = new LegalIdentifier();
		li.setCode("ECLI:IT:COST:1992:51");
		li.setType("ECLI");
		r.getLegalIdentifier().add(li);
		dummyLegalReferences.getLegalReference().add(r);

		r = new LegalReference();
		r.setText("sentenza 29 maggio 1997, nella causa C-299/95, Kremzow");
		r.setIsCaseLawReference(true);
		r.setContext(
				"Corte di giustizia dell'Unione Europea sia anteriore che successivo all'entrata in vigore del Trattato di Lisbona (cfr., ex plurimis, la sentenza 29 maggio 1997, nella causa C-299/95, Kremzow");
		r.setAuthority(CommonCaseLawAuthorities.CJEU.toString());
		r.setType(CommonCaseLawTypes.JUDGMENT.toString());
		r.setCaseYear(Api.toYear("1995"));
		r.setCaseNumber("299");
		xmlDate = datatypeFactory.newXMLGregorianCalendar("1997-05-29");
		r.setDay(xmlDate);
		r.setMonth(xmlDate);
		r.setYear(xmlDate);
		r.setApplicant("Kremzow");
		dummyLegalReferences.getLegalReference().add(r);

		r = new LegalReference();
		r.setText("TFUE art.12");
		r.setAlias(CommonLegislationAliases.TREATY_FUNCTIONING_EU.toString());
		r.setPartition("12");
		li = new LegalIdentifier();
		li.setType("CELEX");
		li.setCode("12012E");
		r.getLegalIdentifier().add(li);
		dummyLegalReferences.getLegalReference().add(r);

		r = new LegalReference();
		r.setType("ECLI");
		r.setIsCaseLawReference(true);
		r.setText("EU:C:2013:48");
		li = new LegalIdentifier();
		li.setCode("ECLI:EU:C:2013:48");
		li.setType("ECLI");
		r.getLegalIdentifier().add(li);
		dummyLegalReferences.getLegalReference().add(r);

		return dummyLegalReferences;
	}
}
