package eu.boecli.engineapi;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestEntryPoint extends HttpServlet {

	private static final long serialVersionUID = -7385504404011452446L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String path = req.getRequestURI();
		if (!path.startsWith("/test/")) {
			resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
		if (path.endsWith("/")) {
			path = path + "index.html";
		}
		path = path.substring(6);
		Path base = Paths.get("test");
		Path sub = base.resolve(path);
		if (Files.isReadable(sub)) {
			try {
				resp.setContentLength((int) sub.toFile().length());
				Files.copy(sub, resp.getOutputStream());
			} catch (IOException e) {
				resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
		} else {
			try {
				InputStream in = Validator.class
						.getResourceAsStream("/" + path);
				if (in == null) {
					resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
					return;
				}
				OutputStream out = resp.getOutputStream();
				byte[] buffer = new byte[1024];
				int len;
				while ((len = in.read(buffer)) != -1) {
					out.write(buffer, 0, len);
				}
			} catch (IOException e) {
				resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
		}
	}

}
