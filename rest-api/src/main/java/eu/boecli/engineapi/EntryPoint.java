package eu.boecli.engineapi;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.xml.sax.SAXException;

import eu.boecli.engineapi.Validator.Direction;
import eu.boecli.engineapi.schema.TextInput;
import eu.boecli.engineapi.schema.TextOutput;

/**
 * EntryPoint exposes the java API Api as a REST API. All input and output is
 * validated against the XML Schema 'schema.xsd'. All functionality is available
 * as XML and JSON.
 */
@Path("entry-point")
public class EntryPoint {

	final private Validator validator;
	final private Api api;

	public EntryPoint() throws SAXException {
		validator = new Validator();
		api = new Api();
	}

	@POST
	@Path("run")
	@Consumes(MediaType.TEXT_XML)
	@Produces(MediaType.TEXT_XML)
	public TextOutput run_xml(@Valid TextInput input) {
		System.out.println("run");
		validator.validate(Direction.INCOMING, input, TextInput.class);
		TextOutput output = api.run(input);
		return validator.validate(Direction.OUTGOING, output, TextOutput.class);
	}

	@POST
	@Path("run")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public TextOutput run_json(@Valid TextInput input) {
		return run_xml(input);
	}
}
