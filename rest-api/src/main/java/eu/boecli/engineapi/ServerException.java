package eu.boecli.engineapi;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class ServerException extends WebApplicationException {

	private static final long serialVersionUID = -6140930137911476978L;

	public ServerException(Throwable t) {
		super(r(t.getMessage()));
	}

	public ServerException(String message, Throwable t) {
		super(r(message + "\n" + t.getMessage()));
	}

	public ServerException(String message) {
		super(r(message));
	}

	static private Response r(String message) {
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
				.entity(message).type("text/plain").build();
	}
}