package eu.boecli.engineapi;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.boecli.common.CommonIdentifiers;
import eu.boecli.engine.BOECLIEngine;
import eu.boecli.engine.BOECLIEngineDocument;
import eu.boecli.engine.BOECLIEngineDocumentFactory;
import eu.boecli.engineapi.schema.EcliReferenceMetadata;
import eu.boecli.engineapi.schema.EcliReferenceMetadata.Reference;
import eu.boecli.engineapi.schema.LegalIdentifier;
import eu.boecli.engineapi.schema.LegalReference;
import eu.boecli.engineapi.schema.References;
import eu.boecli.engineapi.schema.TextInput;
import eu.boecli.engineapi.schema.TextOutput;

public class Api {

	private TextOutput dummy() {
		References refs = Dummy.getDummyLegalReferences();
		TextOutput output = new TextOutput();
		output.setReferences(refs);
		return output;
	}

	private EcliReferenceMetadata getECLIReferenceMetadata(
			BOECLIEngineDocument doc) {

		EcliReferenceMetadata ermd = new EcliReferenceMetadata();
		// TODO Produce the ECLI Metadata for the extracted references

		// for each reference, for each identifier...

		String lang = doc.getLanguage().toString().toLowerCase();

		Set<String> codes = new HashSet<String>();

		for (eu.boecli.reference.LegalReference legalReference : doc
				.getLegalReferences()) {

			Set<String> types = new HashSet<String>();

			for (eu.boecli.reference.LegalIdentifier legalIdentifier : legalReference
					.getLegalIdentifiers()) {

				String type = "OTHER";

				if (legalIdentifier.getIdentifier()
						.equals(CommonIdentifiers.ECLI))
					type = "ECLI";
				if (legalIdentifier.getIdentifier()
						.equals(CommonIdentifiers.ELI))
					type = "ELI";
				if (legalIdentifier.getIdentifier()
						.equals(CommonIdentifiers.CELEX))
					type = "CELEX";

				// One identifier (max score) for each type
				if (types.contains(type))
					continue;
				types.add(type);

				String code = legalIdentifier.getCode();

				// Distinct output
				if (codes.contains(code))
					continue;
				codes.add(code);

				Reference entry = new Reference();
				entry.setCode(code);
				entry.setLang(lang);
				entry.setRelation("citing");
				entry.setType(type);
				ermd.getReference().add(entry);
			}
		}

		return ermd;
	}

	public TextOutput run(@Valid TextInput input) {
		if ("dummy".equals(input.getText())) {
			return dummy();
		}

		BOECLIEngineDocument engineDocument = BOECLIEngineDocumentFactory
				.getDocument(input.getText(), input.getInputEcli(),
						input.getLanguage(), input.getJurisdiction());

		boolean success = BOECLIEngine.run(engineDocument);
		TextOutput output = new TextOutput();
		for (String s : engineDocument.getMessages()) {
			output.getLog().add(s);
		}
		for (String e : engineDocument.getErrors()) {
			output.getError().add(e);
		}
		if (success) {
			output.setAnnotatedText(engineDocument.getAnnotatedText());
			output.setReferences(
					convertReferences(engineDocument.getLegalReferences()));
			output.setEcliReferenceMetadata(
					getECLIReferenceMetadata(engineDocument));
		}
		return output;
	}

	private References convertReferences(
			Collection<eu.boecli.reference.LegalReference> refs) {
		References references = new References();
		for (eu.boecli.reference.LegalReference reference : refs) {
			LegalReference lr = convertLegalReference(reference);
			references.getLegalReference().add(lr);
		}
		return references;
	}

	private LegalIdentifier convertLegalIdentifier(
			eu.boecli.reference.LegalIdentifier in) {
		LegalIdentifier out = new LegalIdentifier();
		out.setType(in.getIdentifier().toString());
		out.setCode(in.getCode());
		out.setProvenance(in.getProvenance());
		out.setUrl(in.getUrl());
		out.setConfidence(Double.valueOf(in.getConfidenceValue()));
		return out;
	}

	private LegalReference convertLegalReference(
			eu.boecli.reference.LegalReference in) {
		LegalReference out = new LegalReference();
		for (eu.boecli.reference.LegalIdentifier i : in.getLegalIdentifiers()) {
			out.getLegalIdentifier().add(convertLegalIdentifier(i));
		}
		out.setText(emptyIsNull(in.getText()));
		out.setContext(emptyIsNull(in.getContext()));
		out.setProvenance(emptyIsNull(in.getProvenance()));
		out.setPartition(emptyIsNull(in.getPartition()));
		out.setAuthority(emptyIsNull(in.getAuthority()));
		out.setType(emptyIsNull(in.getType()));
		out.setSection(emptyIsNull(in.getSection()));
		out.setSubject(emptyIsNull(in.getSubject()));

		String date = emptyIsNull(in.getDate());
		XMLGregorianCalendar xmlDate = toDate(date);
		out.setDay(xmlDate);
		out.setMonth(xmlDate);
		if (date != null && date.length() == 10) {
			// get the year from the date
			out.setYear(toYear(date.substring(0, 4)));
		}
		if (in.getYear() != null) {
			out.setDocYear(toYear(in.getYear()));
		}
		out.setFullNumber(emptyIsNull(in.getNumber(true)));
		out.setNumber(emptyIsNull(in.getNumber()));
		out.setNumberPrefix(emptyIsNull(in.getNumberPrefix()));
		out.setFullCaseNumber(emptyIsNull(in.getCaseNumber(true)));
		out.setCaseNumber(emptyIsNull(in.getCaseNumber()));
		out.setCaseYear(toYear(emptyIsNull(in.getCaseYear())));
		out.setCasePrefix(emptyIsNull(in.getCasePrefix()));
		out.setFullAltNumber(emptyIsNull(in.getAltNumber(true)));
		out.setAltYear(toYear(emptyIsNull(in.getAltYear())));
		out.setAltPrefix(emptyIsNull(in.getAltPrefix()));
		out.setApplicant(emptyIsNull(in.getApplicant()));
		out.setDefendant(emptyIsNull(in.getDefendant()));
		out.setGeographic(emptyIsNull(in.getGeographic()));
		out.setMisc(emptyIsNull(in.getMisc()));
		out.setAlias(emptyIsNull(in.getAliasValue()));
		out.setIsCaseLawReference(in.isCaseLawReference());
		out.setIsLegalReference(in.isLegislationReference());

		return out;
	}

	static public XMLGregorianCalendar toDate(String date) {
		if (date == null || "".equals(date)) {
			return null;
		}
		// convert to gMonthDay which has format '--MM-DD'
		if (date.length() == 5) {
			date = "--" + date;
		}
		DatatypeFactory datatypeFactory;
		try {
			datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new ServerException(e);
		}
		try {
			return datatypeFactory.newXMLGregorianCalendar(date);
		} catch (IllegalArgumentException e) {
			throw new ServerException("Cannot convert date '" + date + "'", e);
		}
	}

	static public XMLGregorianCalendar toYear(String year) {
		if (year == null || "".equals(year)) {
			return null;
		}
		DatatypeFactory datatypeFactory;
		try {
			datatypeFactory = DatatypeFactory.newInstance();
		} catch (DatatypeConfigurationException e) {
			throw new ServerException(e);
		}
		try {
			return datatypeFactory.newXMLGregorianCalendar(year);
		} catch (IllegalArgumentException e) {
			throw new ServerException("Cannot convert year '" + year + "'", e);
		}
	}

	private static String emptyIsNull(String s) {
		return "".equals(s) ? null : s;
	}
}
