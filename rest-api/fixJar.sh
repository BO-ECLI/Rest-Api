#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail

rm -rf fixJar_tmp
rm -f target/restapi.jar
mkdir fixJar_tmp
unzip -d fixJar_tmp lib/boecli-italy-0.5.5.jar
unzip -o -d fixJar_tmp lib/boecli-spain-0.2.2.jar
unzip -o -d fixJar_tmp lib/boecli-engine-0.9.1.jar
unzip -o -d fixJar_tmp target/WS-2-0.1-SNAPSHOT.jar
cd fixJar_tmp
zip -r ../target/restapi.jar .
cd -
rm -r fixJar_tmp
