# REST-API

This folder documents the REST-API for communicating with the engine.

The tests in the top-level tests folder are described in terms of this interface. This API is the public interface for the engine.
