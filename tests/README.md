# Tests

This folder contains tests that serve as examples and as quality control for the BO-ECLI WS-2 engine.

Each test consists of an input message and an output message. When the input message is sent to the engine, the exact output should be returned.

The engine can be installed in various configurations. Different folders serve to test different configurations.
