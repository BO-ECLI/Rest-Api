# Rest-API

Rest-API is a project that creates an Http API interface to the BO-ECLI Parser Engine

The software exposes a REST API to which text can be sent. The text is annotated and returned as XML or JSON

# Building

The code can be built with maven:
```bash
cd rest-api
mvn install
```

To update the dependencies, run:
```bash
mvn versions:display-dependency-updates
```

# Testing

Start the service:

```bash
cd rest-api
mvn install
java -cp lib/boecli-engine-1.0.3.jar:lib/boecli-italy-0.6.5.jar:lib/boecli-spain-0.3.5.jar:target/WS-2-0.1-SNAPSHOT.jar eu.boecli.engineapi.App
```

Send a test to the service:

```bash
$ cat > test.xml
<bo:text-input xmlns:bo="http://boecli.eu/engine/0.1" language="IT">
    <bo:text>hello world</bo:text>
</bo:text-input>
$ curl -v -H "Accept: text/xml" -H "Content-Type: text/xml" --data @test.xml http://localhost:8080/entry-point/run
```

```bash
$ cat > test.json
$ curl -v -H "Accept: application/json" -H "Content-Type: application/json" --data @test.json http://localhost:8080/entry-point/run
```
